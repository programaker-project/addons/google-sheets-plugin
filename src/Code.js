function onOpen(e) {
  const ui = SpreadsheetApp.getUi();

  ui.createAddonMenu() // Or DocumentApp.
      .addItem('Get custom formulas', 'openPanel')
      .addToUi();
}

function onInstall(e) {
  onOpen(e);
}

function openPanel(opts) {
  const ui = SpreadsheetApp.getUi();
  const template = HtmlService.createTemplateFromFile('Sidebar');

  const userProps = PropertiesService.getUserProperties();

  template.props = userProps;
  template.logged = !!userProps.getProperty('login-token');

  if (opts && opts.error) {
    template.error = opts.error;
  } 
  else {
    template.error = null;
  }
   

  if (template.logged) {
    const services = JSON.parse(userProps.getProperty('services'));
    const blocks = JSON.parse(userProps.getProperty('blocks'));

    if (services && blocks) {
      template.services = services;
      template.blocks = blocks;
    }
    else {
      template.logged = false; // Error on login
    }
  }

  const html = template.evaluate()
    .setTitle('PrograMaker')
    .setWidth(300);
  ui.showSidebar(html);
}

/**
 * @customfunction
 * Extract a property from a JSON element.
 * @param {string} value JSON value.
 * @param {string|number} key key to extract.
 * @return {any} Value on the given key.
 */
function JGET(value, key) {
  const data = JSON.parse(value);

  if (value.trim().startsWith('[')) {
    key = parseInt(key);
  }

  return data[key];
}

/**
 * @customfunction
 * Format and indent a JSON value.
 * @param {string} value JSON value.
 * @return {string} Indented JSON format.
 */
function JFORMAT(value) {
  const data = JSON.parse(value);

  return JSON.stringify(data, null, 4);
}

/**
 * @customfunction
 * Use PrograMaker to pull information from 3rd party APIs.
 * @param {string} back_function
 * @param {any[]} args
 * @return {any} Value returned by the 3rd party.
 */
function PRMKR(back_function, ...args) {
  const userProps = PropertiesService.getUserProperties();

  const m = back_function.match(/^(.*)_([0-9A-Fa-f]+)$/);
  const function_call = m[1];

  const services = JSON.parse(userProps.getProperty('services'));
  const bridge_id = services.find(serv => serv.bridge_id.startsWith(m[2])).bridge_id;
  
  const url = `https://programaker.com/api/v0/users/id/${userProps.getProperty('session-user-id')}/bridges/id/${bridge_id}/functions/${function_call}`;
  const token = userProps.getProperty("login-token");

  const parsed = args.map(arg => {
    try {
      return JSON.parse(arg);
    }
    catch (err) {
      return arg;
    }
  });

  const response = UrlFetchApp.fetch(url, {
    'method' : 'post',
    'contentType': 'application/json',
    // Convert the JavaScript object to a JSON string.
    'payload' : JSON.stringify({arguments: parsed}),
    headers: {
      'AUTHORIZATION': token,
    }
  });
  const text = response.getContentText();
  const data = JSON.parse(text).result;

  return serialize(data);
}

function serialize(data) {
  if (['number', 'string', 'boolean'].indexOf(typeof(data)) >= 0) {
    return data;
  }
  if (Array.isArray(data)) {
    return data.map((v) => serialize(v));
  }

  return JSON.stringify(data);
}

function refresh() {
  const userProps = PropertiesService.getUserProperties();
  if (!!userProps.getProperty('login-token')) {
    updateAllInfo();
  }

  openPanel();
}

function startLogin() {
  const ui = SpreadsheetApp.getUi();

  const html = HtmlService.createHtmlOutputFromFile('LoginDialog')
    .setTitle('Login to PrograMaker')
    .setWidth(300);
  ui.showModalDialog(html, 'Login to PrograMaker');
}

function finishLogin(token) {
  const userProps = PropertiesService.getUserProperties();
  userProps.setProperty("login-token", token);

  try {
    updateAllInfo();
    openPanel();
  }
  catch (err) {
    openPanel({ error: err.message });
  }
}

function logout() {
  const userProps = PropertiesService.getUserProperties();
  userProps.deleteAllProperties();
  openPanel();
}