function buildInputForArgument(block_id, block, match, bridge_id) {
  const argNum = parseInt(match.substr(1)) - 1;
  const arg = block.arguments[argNum];
  if (!arg) {
    return '';
  }

  if (!arg.callback) { // TODO: Handle callback sequences
    let type = 'text';

    if (['number', 'integer', 'float'].indexOf(arg.type) >= 0) {
      type = 'number';
    }

    return `<input class="argument" type="${type}" onkeyup="updateCommand('${block_id}_${argNum + 1}', this)" placeholder="Arg ${argNum + 1}" />`
  }

  try {
    const options = getArgOptions(bridge_id, arg);
    
    const acc = [`<select class="argument" onchange="updateCommand('${block_id}_${argNum + 1}', this)">`];
      acc.push(`<option value='' selected>- Select -</option>`);

    for (const [label, id] of options) {
      acc.push(`<option value='${id}'>${label}</option>`);
    }
    acc.push('</select>');
    return acc.join('\n');
  }
  catch (error) {
    console.error(`Error obtaining ${bridge_id}.${arg.callback}`);
    return `
      <select class="argument unavailable" disabled="true">
        <option>Not available</option>
      </select>`;
  }

}

function buildLeftOverArguments(block_id, block, bridge_id) {
  const regexp = RegExp(/%\d/g);

  let arguments_in_message = 0;
  while ((regexp.exec(block.message)) !== null) {
    arguments_in_message++;
  }

  const acc = [];
  while (arguments_in_message < block.arguments.length) {
    acc.push(buildInputForArgument(block_id, block, '%' + (arguments_in_message + 1), bridge_id));
    arguments_in_message++;
  }

  return acc.join('');
}