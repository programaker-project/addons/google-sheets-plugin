

function updateSession(token) {
  const url = `https://programaker.com/api/v0/sessions/check`;
  let response;
  
  try {
    response = UrlFetchApp.fetch(url, {
      'method' : 'get',
      'headers': {
        'AUTHORIZATION': token,
      }
      });
  }
  catch (error) {
    console.error("Error", error);
    throw Error("Invalid authentication token. Please try to login again.");
  }

  const data = JSON.parse(response.getContentText());
  if (!data.success) {
    console.error("No success when checking session");
    throw Error("Invalid authentication token. Please try to login again.");
  }

  const userProps = PropertiesService.getUserProperties();
  userProps.setProperties({
    'session-username': data.username,
    'session-user-id': data.user_id,
    'session-tags-in-preview': data.tags.is_in_preview,
    'session-tags-is-advanced': data.tags.is_advanced,
    'session-tags-is-admin': data.tags.is_admin,
  });

  return data;
}

function updateServices(props) {
  const url = `https://programaker.com/api/v0/users/id/${props.getProperty('session-user-id')}/connections/established`;
  const response = UrlFetchApp.fetch(url, {
    'method' : 'get',
    'headers': {
      'AUTHORIZATION': props.getProperty('login-token'),
    }
  });

  return response.getContentText();
}

function updateBlocks(props) {
  const url = `https://programaker.com/api/v0/users/${props.getProperty('session-username')}/custom-blocks`;
  const response = UrlFetchApp.fetch(url, {
    'method' : 'get',
    'headers': {
      'AUTHORIZATION': props.getProperty('login-token'),
    }
  });

  return response.getContentText();
}

function updateAllInfo() {
  const userProps = PropertiesService.getUserProperties();
  const token = userProps.getProperty("login-token");

  updateSession(token);
  const blocks = updateBlocks(userProps);
  const services = updateServices(userProps);
  userProps.setProperties({
    blocks: blocks,
    services: services,
  })
}

function getArgOptions(bridge_id, arg) {
  const userProps = PropertiesService.getUserProperties();
  const url = `https://programaker.com/api/v0/users/id/${userProps.getProperty('session-user-id')}/bridges/id/${bridge_id}/callback/${arg.callback}`;
  const response = UrlFetchApp.fetch(url, {
    'method' : 'get',
    'headers': {
      'AUTHORIZATION': userProps.getProperty('login-token'),
    }
  });

  return reformatCallbackResult(JSON.parse(response.getContentText())); 
}

function reformatCallbackResult(result) {
  const options = [];
  if (result.constructor == Object) {
      // Data from callback as dictionary
      const resultDict = result;

      for (const key of Object.keys(resultDict)) {
          options.push([resultDict[key].name || key, key]);
      }
  }
  else {
      // Data from callback as list
      const resultList = result;

      for (const item of resultList) {
          options.push([item.name, item.id]);
      }
  }
  return options;
}
